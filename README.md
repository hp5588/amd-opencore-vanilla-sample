## A successful Hackintosh (Ryzentosh) Build Up
![Screenshot](screenshot.jpg "Ryzentosh macOS Catalina 10.15.4")
### Computer Hardware
+ CPU: AMD Ryzen 5 3600
+ RAM: ADATA XPG GAMMIX D10 DDR4 3200MHz 16GB * 2
+ MB: msi B450M MORTAR MAX 
+ Graphic: Radeon RX 580 ARMOR 4G OC
+ SSD: UMAX S350 256GB 

### Software
+ OS: macOS Catalina 10.15.4
+ Boot: OpenCore 0.5.7 (**version is important**)
+ Vanilla Patch: https://github.com/AMD-OSX/AMD_Vanilla  commit 33a11a288b3c319f7561d78879d639aa802b9b67

### Preperation
1. Get a thumb drive (I had 32 GB one)
1. Make sure you have a backup of data on target drive 
1. Watching this https://www.youtube.com/watch?v=4nr4XjD7AHg&t=1557s before you go into detail can give you an overview
    BUT video above is **not working** in my case due to different OpenCore version (will explain later)
    
    I found a newer one https://www.youtube.com/watch?v=QtvDRDqgolc seems to work (not tested)

1. https://dortania.github.io/OpenCore-Desktop-Guide/ktext.html
This page help later when you are looking for you own drivers and KEXTs

1. Assume our thumb drive is label F:

1. `config.plist` (locate in *F:\EFI\OC*) will be mentioned multiple time in the following paragraph


### Steps
1. **gibMacOS** `git clone https://github.com/corpnewt/gibMacOS.git`
This repository allows you to download macOS images directly from Apple (no more torrents and MediaFire)
follwing commands runs on Windows (need Python)
`gibMacOS.bat` (download image)
`MakeInstall.bat` (install macOS image onto USB stick)
refer to the video in [Preperation]()

1. **OpenCorePkg** `git clone https://github.com/acidanthera/OpenCorePkg.git`
*make sure you checkout to tag `0.5.7`*
*OpenCorePkg/Docs/Configuration.pdf* is a very important reference file that help to setup `config.plist` correctly
\*Notice: OpenCore should have been installed on thumb drive by [gibMacOS](), here we only need the documentation.

1. **AMD Vanilla Patches** `git clone https://github.com/AMD-OSX/AMD_Vanilla.git`
*make sure you checkout to branch `opencore`*
*AMD_Vanilla/17h/patches.plist* is the patch mentioned in video which will be copied into `config.plist` later with tool `ProperTree`

1. **SSDTTime** `git clone https://github.com/corpnewt/SSDTTime`
Generate your own files for ACPI folder (*F:\EFI\OC\ACPI*)

1. Pickup your own drivers and KEXTs from 
https://dortania.github.io/OpenCore-Desktop-Guide/ktext.html
    > The only extra kext I added is `RealtekRTL8111.kext` (internet connection for Realtek NIC)

1. **ProperTree** `git clone https://github.com/corpnewt/ProperTree.git`
Tool for modification of `.plist` file.
`OC Snapshot (Ctrl + R)`: help you readout KEXTs and drivers in your selected *OC/* folder and set entries in `config.plist`.
`Save (Ctrl + S)`: save changes into file `config.plist`.
    > IMPORTANT: Whenever you are adding or removing KEXTs and drivers, you need to repeat this action so that changes will be written in to config.plist and will take effect in next boot.
1. **[IMPORTANT]** Manually Tune `config.plist`
Following all steps in video, expect `RequireSignature` and `RequireVault`. Since OpenCore **0.5.6**, these two attributes are removed and replaced by `Vault`. Therefore, set key `Vault` with string value `Optional` instead. You can refer to my `config.plist` file for details.  Besides, `FwRuntimeServices` is renamed to `OpenRuntime` since **0.5.7** and will cause error if using old `config.plist' template. This should correspond to file in your driver folder *F:\EFI\OC\Drivers\OpenRuntime.efi (or FwRuntimeServices.efi)*


Compare content of your EFI folder with this repository (F:\EFI)
They should looks similar
    
    > F:\
    │   boot
    │   README.md
    │   Configuration.pdf
    │
    └───EFI
        ├───BOOT
        │       BOOTx64.efi
        │
        ├───OC
        │   │   OpenCore.efi
        │   │   config.plist.old
        │   │   patches.plist
        │   │   config.plist
        │   │
        │   ├───ACPI
        │   │       DSDT.aml
        │   │       SSDT-EC.aml
        │   │       SSDT-EC.dsl
        │   │
        │   ├───Drivers
        │   │       ApfsDriverLoader.efi
        │   │       VBoxHfs.efi
        │   │       OpenRuntime.efi
        │   │
        │   ├───Kexts
        │   │   ├───AppleALC.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       └───MacOS
        │   │   │               AppleALC
        │   │   │
        │   │   ├───Lilu.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       └───MacOS
        │   │   │               Lilu
        │   │   │
        │   │   ├───NullCPUPowerManagement.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       ├───MacOS
        │   │   │       │       NullCPUPowerManagement
        │   │   │       │
        │   │   │       └───Resources
        │   │   │           └───English.lproj
        │   │   │                   InfoPlist.strings
        │   │   │
        │   │   ├───SmallTreeIntel82576.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       ├───MacOS
        │   │   │       │       SmallTreeIntel82576
        │   │   │       │
        │   │   │       ├───Resources
        │   │   │       │   └───English.lproj
        │   │   │       │           InfoPlist.strings
        │   │   │       │
        │   │   │       └───_CodeSignature
        │   │   │               CodeResources
        │   │   │
        │   │   ├───VirtualSMC.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       └───MacOS
        │   │   │               VirtualSMC
        │   │   │
        │   │   ├───WhateverGreen.kext
        │   │   │   └───Contents
        │   │   │       │   Info.plist
        │   │   │       │
        │   │   │       └───MacOS
        │   │   │               WhateverGreen
        │   │   │
        │   │   └───RealtekRTL8111.kext
        │   │       └───Contents
        │   │           │   Info.plist
        │   │           │
        │   │           ├───MacOS
        │   │           │       RealtekRTL8111
        │   │           │
        │   │           └───Resources
        │   │               └───en.lproj
        │   │                       InfoPlist.strings
        │   │
        │   └───Tools
        └───Resources
            ├───Audio
            ├───Font
            ├───Image
            └───Label

Follow video to go through the rest of macOS installation  :)

https://mackie100projects.altervista.org/download-clover-configurator/
-> writting efi booting file to taget installation using clover configurator (so that you can boot without USB stick)